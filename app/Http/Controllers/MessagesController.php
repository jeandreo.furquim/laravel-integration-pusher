<?php

namespace App\Http\Controllers;

use App\Events\ChatMessageEvent;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Message $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        // $contents = Message::orderBy('name', 'ASC')->get();

        // RETURN VIEW WITH DATA
        return view('pages.messages.index', [
            // 'contents' => $contents,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $message = $request->text;
        $user = Auth::user();
        
        // Envie o evento de broadcast
        broadcast(new ChatMessageEvent($message))->toOthers();

        return response()->json(['message' => 'Mensagem enviada com sucesso']);

    }

}