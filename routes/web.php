<?php

use App\Http\Controllers\MessagesController;
use App\Http\Controllers\VideoRTCController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// SULINK STORES
Route::prefix('/')->group(function () {
    Route::name('messages.')->group(function () {
        Route::get('/', [MessagesController::class, 'index'])->name('index');
        Route::get('/adicionar', [MessagesController::class, 'create'])->name('create');
        Route::post('/adicionar', [MessagesController::class, 'store'])->name('store');
        Route::get('/visualizando/{id}', [MessagesController::class, 'show'])->name('show');
        Route::get('/desabilitar/{id}', [MessagesController::class, 'destroy'])->name('destroy');
        Route::get('/editar/{id}', [MessagesController::class, 'edit'])->name('edit');
        Route::put('/editar/{id}', [MessagesController::class, 'update'])->name('update');
    });
});

// SULINK STORES
Route::prefix('/video')->group(function () {
    Route::name('video.')->group(function () {
        Route::get('/', [VideoRTCController::class, 'index'])->name('index');
        Route::get('/adicionar', [VideoRTCController::class, 'create'])->name('create');
        Route::post('/adicionar', [VideoRTCController::class, 'store'])->name('store');
        Route::get('/visualizando/{id}', [VideoRTCController::class, 'show'])->name('show');
        Route::get('/desabilitar/{id}', [VideoRTCController::class, 'destroy'])->name('destroy');
        Route::get('/editar/{id}', [VideoRTCController::class, 'edit'])->name('edit');
        Route::put('/editar/{id}', [VideoRTCController::class, 'update'])->name('update');
    });
});

Route::post('/broadcasting/auth', function () {
    return Auth::user();
});