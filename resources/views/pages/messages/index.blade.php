@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow border-0">
            <div class="card-body">
                <h2 class="text-center fs-4 text-uppercase fw-bold py-2">Chat em tempo real</h2>
                <div class="w-100 rounded bg-light p-4" style="min-height: 400px">
                    <div class="msg-chat">
                        {{-- <span class="p-2 rounded bg-white shadow-lg">
                            Olá, tudo bem?
                        </span> --}}
                    </div>
                </div>
                <div class="text-end">
                    <form action="/" method="post" enctype="multipart/form-data" id="form-message">
                        @csrf
                        <input type="text" name="message" id="" class="form-control mt-3 bg-light">
                        <button type="submit" class="btn btn-sm btn-success mt-2 px-4 fw-bold">ENVIAR</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-footer')
<script>
    $(document).ready(function() {

        $(document).on('submit', '#form-message', function(e){

            e.preventDefault();

            // GET ROUTE
            var url = "{!! route('messages.store') !!}";
            var text = $(this).find('[name="message"]').val();

            $.ajax({
                type:'POST',
                url: url,
                data: {
                    _token: @json(csrf_token()), 
                    text: text,
                },
                success:function(data) {
                    $('[name="message"]').val('');
                    $('.msg-chat').append('<p class=""><span class="p-2 rounded bg-white shadow-lg">Eu: ' + text + '</span></p>');
                }
            });

        });

        Echo.channel(`chat-channel`).listen('ChatMessageEvent', (e) => {
            console.log(e);
            $('.msg-chat').append('<p class="text-end"><span class="p-2 rounded bg-white shadow-lg">Cliente: ' + e.message + '</span></p>');
        });
    });

</script>
@endsection